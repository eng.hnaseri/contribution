---
title: 
title-en: How to Improve Your Study Skills - Video & Lesson Transcript | Study.com
description: 
description-en: Everyone knows that they should study, but many students aren't sure exactly how to study. In this lesson, we'll talk about study skills, including how to arrange your study schedule and how to study effectively.
---

## Before Studying

Hector knows he needs to study, but he's not sure how to find the time or what he should do to plan out his study sessions so that they are most effective. When should he study? How does he know what to study? It all seems overwhelming! 

Before Hector begins studying, he can set himself up for success through planning out his study sessions. There are four things that Hector needs to do, and he can remember them with the first letter of each word in the sentence, **S** ome **P** eople **B** elieve **P** erjury. 

**S** is for **schedule**. Hector will want to schedule regular study sessions. Preferably, he'll want to set aside time every day to study. Research has shown that breaking study time up into small chunks spread out over days or weeks is more effective than cramming it all into one long study session right before a test. If Hector can find 30 to 60 minutes a day to study, it will help him tremendously. 

**P** is for **plan**. Everyone has a time of day that they are most awake. For example, Hector is a night owl. He likes to stay up late into the evening and is most alert at that time. On the other hand, his friend, Eva, is a morning person and gets up really early every day, because she is awake at that time. 

Hector should plan his study sessions so that they are in line with his natural rhythms. For him, nighttime is the perfect time to study, because he is alert and awake. He should especially try to set aside his peak time for his most difficult topics. If he has extra time to study, he should consider using that time to review. 

**B** is for **break up**. No, I'm not talking about Hector's relationship status! Hector will want to break up big tasks into more manageable pieces. For example, if he's studying for a test on 20th century history, he won't want to try to study everything all at once. Instead, he might do one study session on the first decade, and another one on the next decade, and so on. 

**P** is for **prioritize**. Hector needs to know what is most important to study. As we've already seen, he'll want to study the hard stuff during his peak time of day. But, he should also start each study session with the most important thing he wants to study. He can make a priority list with the most important things at the top and the less important study topics down below. If he starts at the top and doesn't get to everything, at least he's studied the most important topics. 

## During Studying

OK, Hector is on the right track. He has scheduled study time, planned for it to be at the best time of day for him, broken big topics up into more manageable chunks, and prioritized those chunks so that he can start with the most important topics. 
