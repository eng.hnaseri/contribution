## Study Skills

Hector is struggling at school. He's taking an online class and isn't understanding things as well as when he used to take classes in person. Then, he could ask lots of questions of the teachers, and he could visit them during office hours. But now, he feels stuck. 

Whether you take classes online, like Hector, or in person, having good **study skills** , or strategies for how to learn material, can help you get good grades. But what are good study skills? And how can Hector and other students develop them? 

Let's look at things that Hector can do both before studying and during studying to set himself up for success. 

## Before Studying

Hector knows he needs to study, but he's not sure how to find the time or what he should do to plan out his study sessions so that they are most effective. When should he study? How does he know what to study? It all seems overwhelming! 

Before Hector begins studying, he can set himself up for success through planning out his study sessions. There are four things that Hector needs to do, and he can remember them with the first letter of each word in the sentence, **S** ome **P** eople **B** elieve **P** erjury. 

**S** is for **schedule**. Hector will want to schedule regular study sessions. Preferably, he'll want to set aside time every day to study. Research has shown that breaking study time up into small chunks spread out over days or weeks is more effective than cramming it all into one long study session right before a test. If Hector can find 30 to 60 minutes a day to study, it will help him tremendously. 

**P** is for **plan**. Everyone has a time of day that they are most awake. For example, Hector is a night owl. He likes to stay up late into the evening and is most alert at that time. On the other hand, his friend, Eva, is a morning person and gets up really early every day, because she is awake at that time. 

Hector should plan his study sessions so that they are in line with his natural rhythms. For him, nighttime is the perfect time to study, because he is alert and awake. He should especially try to set aside his peak time for his most difficult topics. If he has extra time to study, he should consider using that time to review. 

**B** is for **break up**. No, I'm not talking about Hector's relationship status! Hector will want to break up big tasks into more manageable pieces. For example, if he's studying for a test on 20th century history, he won't want to try to study everything all at once. Instead, he might do one study session on the first decade, and another one on the next decade, and so on. 

**P** is for **prioritize**. Hector needs to know what is most important to study. As we've already seen, he'll want to study the hard stuff during his peak time of day. But, he should also start each study session with the most important thing he wants to study. He can make a priority list with the most important things at the top and the less important study topics down below. If he starts at the top and doesn't get to everything, at least he's studied the most important topics. 

## During Studying

OK, Hector is on the right track. He has scheduled study time, planned for it to be at the best time of day for him, broken big topics up into more manageable chunks, and prioritized those chunks so that he can start with the most important topics. 

So, is that it? Does he just sit down and…what? What exactly should he do during a study session? 

Just as there are certain things that Hector can do before he studies, there are certain things he should do during studying in order to make his study time the most effective it can be. 

At the beginning of a topic, Hector should preview the topic. He can do this by looking through notes, his textbook, any online videos, and other resources for major headlines. At this point, he's not reading through everything, just glancing through to see what the major headlines or topics are. 

Once he's previewed the topic, he's ready to dive in and look at things closer. This is when he can reread his notes or textbook, watch (or re-watch) videos, and examine other resources in-depth. As he's doing this, it's important that he check for understanding at regular intervals. Is he understanding what he's reading or watching? Does he have unanswered questions? 

He can also keep his motivation high by setting up a reward system using breaks and snacks to mark certain milestones. For example, if Hector finishes studying a chapter in his textbook, he might want to reward himself with an apple or another snack. After he masters a topic, he can take a break to check email or go online for five minutes. 

Even if he hasn't earned a reward, though, Hector will want to take breaks to move every 45 to 60 minutes. These breaks don't have to be long; a couple of minutes of push-ups or dancing to one song will get his blood flowing and keep him energized and focused. Studies show that moderate exercise, like a walk, improves memory and helps people retain information better, so he should try to move regularly. 

Finally, after Hector finishes with a topic or a study session, he will want to quiz himself to make sure he really understands. He can use ready-made quizzes, like those at the end of a textbook chapter or the end of online videos, or he can make up his own flashcards or quizzes. But, he'll want to make sure that he is testing himself, because that can help him remember things better. 

## Lesson Summary

**Study skills** are strategies used to help you learn something. Before studying, strategies include scheduling time to study every day, planning to study at the best time (including the time that you are most alert), breaking big topics into manageable chunks, and prioritizing study topics to ensure you are studying the most important topics first. During a study session, the best approach is to preview a topic first, check for understanding as you study, set up a reward system using breaks and snacks, take breaks to move every 45 to 60 minutes, and quiz yourself at the end of the study session to make sure you remember the information. 

## Learning Outcomes

After viewing and memorizing this lesson, you might be ready to: 

  * Recognize the need for study skills 
  * Understand the meaning behind the 'Some People Believe Perjury' acronym 
  * Outline processes that help during studying


